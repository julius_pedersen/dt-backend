import setuptools

setuptools.setup(
    name='dt',
    version='0.0.1',
    author='Julius Pedersen',
    author_email='deifyed@gmail.com',
    url='n/a',
    description='a set of tools for backend development',
    packages=setuptools.find_packages(),
    classifiers=[
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
    ]
)
