import datetime

from django.conf import settings
from django.db import models

class ResourceMixin():
    @property
    def collection_url(self):
        return '{base_url}/{resource_name}/'.format(**{
            'base_url': settings.BASE_URL,
            'resource_name': self.__class__.__name__.lower() + 's',
        })
    @property
    def url(self):
        if not hasattr(self, 'id'):
            self.id = getattr(self, self.__class__._meta.pk.name)
        return self.collection_url + str(self.id) + '/'

    def patch(self, data):
        for attr in data.keys():
            if hasattr(self, attr):
                setattr(self, attr, data[attr])
            else:
                raise AttributeError()

    def serialize(self):
        serialized_data = dict()

        public_fields = '_{}__public_fields'.format(self.__class__.__name__)

        for field in getattr(self, public_fields):
            value = getattr(self, field)
            target = None

            if isinstance(value, (str, int)):
                target = value 
            elif isinstance(value, models.Model):
                target = value.id
            elif isinstance(value, models.manager.Manager):
                target = [item.id for item in value.all()]
            elif isinstance(value, datetime.date):
                target = str(value)

            serialized_data[field] = target

        serialized_data['links'] = [
            {
                'rel': 'self', 
                'href': self.url, 
            },
        ]

        return serialized_data
