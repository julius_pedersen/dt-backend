import json

from django.http import JsonResponse

def login_required(f):
    '''
		Makes sure the user is authenticated, if not returns appropriate
        JsonResponse. The key here is JsonResponse.
    '''
    def authentication_check(*args, **kwargs):
        request = args[0] if len(args) == 1 else args[1]
        print(args[0])

        print(request.user)
        if request.user.is_authenticated:
            return f(*args, **kwargs)
        return JsonResponse(dict(), status=401)
    return authentication_check

def methods(l):
    '''
        Denies unsupported methods with an apropriate json response
    '''
    normalized_allowed_methods = [m.upper() for m in l]

    def mf_wrapper(f):
        def method_filter(request, *args, **kwargs):
            method = request.method.upper()

            if method not in normalized_allowed_methods:
                return JsonResponse(dict(), status=405)
            return f(request, method, *args, **kwargs)

        return method_filter
    return mf_wrapper

def json_data(f):
    def extract_data(*args, **kwargs):
        request = args[0] if len(args) == 1 else args[1]

        args = args + (json.loads(request.body.decode('utf-8')),)

        return f(*args, **kwargs)

    return extract_data
